const path = require('path');
const express = require('express');
const morgan = require('morgan');
const rateLimit = require('express-rate-limit');
const helmet = require('helmet');
const mongoSanitize = require('express-mongo-sanitize');
const xss = require('xss-clean');
//  g) const hpp = require('hpp');


const globalErrorHandler = require('./controller/errorController');
const AppError = require('./utils/appError');

const viewRouter = require('./route/viewRoutes');
const reservationRouter = require('./route/revervationRoutes');

const app = express();

app.set('view engine', 'pug');
app.set('views',path.join(__dirname, 'views'));


// serving static files
app.use(express.static(path.join(__dirname, 'public')));

app.use(express.json());

// 1- GLOBAL MIDDLEWARES
//----------------------------------------------------------------
// a) set security for http headers
// b) development logging
// c) limiting request from same API
// d) body parser, reading data from body into req.body
// e) data sanatization against NOSQL query injection
// f) data sanatization against xss (cross site script attack)
// g) prevent http parameters pollution
//----------------------------------------------------------------

// --> a
app.use(helmet());

// --> b
// development logging
console.log(process.env.NODE_ENV);
if(process.env.NODE_ENV === 'development'){
    app.use(morgan('dev'));
}


// --> c
const limiter = rateLimit({
    max: 1000,
    windowMs: 60 * 60 * 1000,
    message: "Trop de demandes sur cette adresse, veuillez réessayer dan une heure"
});
app.use('/api', limiter);

// --> d
// app.use(express.json({ limit: '100kb'}));

// --> e
app.use(mongoSanitize());

// --> f
app.use(xss());

// --> g
// app.use(hpp({
//     whitelist: []
// }))

app.use((req, res, next) => {
    req.requestTime = new Date().toISOString();

    next();
});

// 1- ROUTES
//----------------------------------------------------------------
app.use('/', viewRouter);
app.use('/api/v1/reservations', reservationRouter);


app.all('*', (req, res, next) => {
    next(new AppError(`Désolé, nous n'avons pas trouvé de correspondance pour ${req.originalUrl}`, 404));
});

app.use(globalErrorHandler);
module.exports = app;