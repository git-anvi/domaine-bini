
import '@babel/polyfill';
import { addReservation } from './reservation';


const reservationForm = document.querySelector('.form--reservation');

if(reservationForm){

    if(document.querySelector('.loading')){
        document.querySelector('.loading').addEventListener('click', e => {
            e.preventDefault();
        });  
    }

    reservationForm.addEventListener('submit', async e => {
        e.preventDefault();
        document.querySelector('.form_cta').innerHTML = `
            <button class="loading">
                <div class="loader"></div>
            </button>
        `
        const lastname = document.getElementById('lastname').value;
        const firstname = document.getElementById('firstname').value;
        const email = document.getElementById('email').value;
        const cellPhone = document.getElementById('cellPhone').value;
        const numberOfPersons = document.getElementById('numberOfPersons').value;
        const dateOfReservation = document.getElementById('dateOfReservation').value;
        const reservedPlan = document.getElementById('reservedPlan').value;
        const message = document.getElementById('message').value;
        
        await addReservation(lastname,firstname,email,cellPhone,numberOfPersons,dateOfReservation,reservedPlan,message)
        
        document.querySelector('.form_cta').innerHTML = `
            <button>
                Reserver
            </button>
        `
        
    })
}