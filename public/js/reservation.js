import axios from 'axios';
import { showAlert } from './alerts';


export const addReservation = async (lastname,firstname,email,cellPhone,numberOfPersons,dateOfReservation,reservedPlan,message) => {
    try {
        const res = await axios({
            method: 'POST',
            url: 'http://127.0.0.1:3000/api/v1/reservations',
            data: {
                lastname,
                firstname,
                email,
                cellPhone,
                numberOfPersons,
                dateOfReservation,
                reservedPlan,
                message
            }
        });
        
        if(res.data.status === 'success'){
            showAlert('success', 'Votre réservation a bien été enregistré !');
            
            window.setTimeout(() => {
                location.assign('/reservation');
            }, 1000)
        }

    } catch (err) {
        showAlert('error', err.response.data.message);
    }
}