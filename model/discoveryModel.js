const mongoose = require('mongoose');

const discoverySchema = mongoose.Schema({
    slug: String,
    title: {
        type: String,
        trim: true,
        lowercase: true,
        required: [true, "Cette information est importante. Merci de spécifier ce qui est à découvrir"],
    },
    mainText: {
        type: String,
        trim: true,
        lowercase: true,
        required: [true, "Cette information est importante. Merci de donner un titre accrocheur"],
    },
    description: {
        type: String,
        trim: true,
        required: [true, "Cette information est importante. Merci d'apporter une description"],
    },
    iconOne: String,
    iconTwo: String,
    images: [String]
});



const Discovery = mongoose.model('Discovery', discoverySchema);
module.exports = Discovery;