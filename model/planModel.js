const mongoose = require('mongoose');

const planSchema = mongoose.Schema({
    slug: String,
    formula: {
        type: String,
        enum: ['découverte', 'immersion', 'team building'],
        required: [true, "Cette information est importante. Dans quelle catégorie placez-vous cette formule ?"]
    },
    description: {
        type: String,
        required: [true, "Cette information est importante. Merci d'apporter une description à cette option"],
    },
    imgCover: {
        type: String,
        required: [true, "Cette information est importante. Merci d'ajouter une bannière"],
    },
    imgIllustration: {
        type: String,
        required: [true, "Cette information est importante. Merci d'ajouter une image d'illustration"],
    },
    price: {
        type: Number,
        required: [true, "Cette information est importante. Merci de spécifier le prix de cette excursion"],
        min: 0
    },
    tourProgram : [
        {
            title: {
                type: String,
                required: [true, "merci de spécifier un titre accrocheur à ce programme"]
            },
            explanation: {
                type: String,
                required: [true, "Nous vous prions de bien vouloir décrire ce programme"]
            },
            illustration: {
                type: String,
                required: [true, "Nous vous prions de bien vouloir ajouter une image d'illustration à ce programme"]
            }
        }
    ],
    tour : {
        type: mongoose.Schema.ObjectId,
        ref: 'Tour',
        required: [true, "A quel tour correspond ce plan d'excursion ?"]
    }
},
{
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
});

// planSchema.pre(/^find/, function(next) {
//     this.populate(
//         {
//             path: 'tour',
//             select: '-__v'
//         }
//     );
  
//     next();
    
//   });


planSchema.pre('save', function(next){
    const name = this.formula.toLowerCase().replace(/\s/g, '-');
    this.slug = 'formule-' + name;
    next();
});

const Plan = mongoose.model('Plan', planSchema);
module.exports = Plan;