const mongoose = require('mongoose');

const teamSchema = mongoose.Schema({
    firstname: {
        type: String,
        trim: true,
        lowercase: true,
        required: [true, "Cette information est importante. Merci de nommer ce membre (prénoms)"],
    },
    lastname: {
        type: String,
        trim: true,
        lowercase: true,
        required: [true, "Cette information est importante. Merci de nommer ce membre (nom)"],
    },
    photo: String,
    role: {
        type: String,
        trim: true,
        lowercase: true,
        required: [true, "Cette information est importante. Merci de spécifier le rôle de ce membre"],
    },
});



const Team = mongoose.model('Team', teamSchema);
module.exports = Team;