const mongoose = require('mongoose');

var validateEmail = function(email) {
    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email)
};

var correctDate = function(dateOfReservation) {
    const currentDate = Date.now();
    if(dateOfReservation < currentDate || dateOfReservation == currentDate)
    {
        return false
    }else{
        return true
    }
};

const reservationSchema = mongoose.Schema({
    firstname: {
        type: String,
        lowercase: true,
        required: [true, "Dites-nous vos prénoms!"],
    },
    lastname: {
        type: String,
        lowercase: true,
        required: [true, "N'oubliez pas votre nom!"],
    },
    email: {
        type: String,
        required: [true, "quel est votre adresse mail?"],
        validate: [validateEmail, 'Email invalide, merci de fournir une adresse correcte'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Email invalide, merci de fournir une adresse correcte']
    },
    cellPhone: {
        type: String,
        required: [true, "Nous avons aussi besoin de votre numéro de téléphone !"]
    },
    numberOfPersons: {
        type: Number,
        min: 1,
        default: 1
    },
    dateOfReservation: {
        type: Date,
        required: [true, "Pour quelle jour planifiez-vous cette réservation ?"],
        validate: [correctDate, 'Date de réservation incorrecte']
    },
    message: {
        type: String,
        trim: true
    },
    reservedPlan: {
        type: mongoose.Schema.ObjectId,
        ref: 'Plan',
        required: [true, "Votre réservation concerne quelle formule ?"]
    },
    isTreated: {
        type: Boolean,
        default: false
    }
});


const Reservation = mongoose.model('Reservation', reservationSchema);
module.exports = Reservation;