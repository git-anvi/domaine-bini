const mongoose = require('mongoose');

const tourSchema = mongoose.Schema({
    name: {
        type: String,
        unique: true,
        trim: true,
        lowercase: true,
        required: [true, "Cette information est importante. Merci de nommer cette formule"],
    },
    slug: String,
    imgCover: {
        type: String,
        required: [true, "Cette information est importante. Merci d'ajouter une image d'illustration"],
    },
    description: {
        type: String,
        required: [true, "Cette information est importante. Merci d'apporter une description"],
    },
    type: {
        type: String,
        enum: ['forest', 'laguna', 'person'],
        required: [true, "Cette information est importante. Dans quelle catégorie placez-vous cette formule ?"]
    }
},
{
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
});


// virtual populate
tourSchema.virtual('tourPlans', {
    ref: 'Plan',
    foreignField: 'tour',
    localField: '_id'
});


tourSchema.pre('save', function(next){
    this.slug = this.name.toLowerCase().replace(/\s/g, '-');
    next();
});



const Tour = mongoose.model('Tour', tourSchema);
module.exports = Tour;