const mongoose = require('mongoose');

const generalSchema = mongoose.Schema({
    concept: {
        type: String,
        trim: true,
        required: [true, "Cette information est importante. Merci de renseigner le concept du domaine bini"],
    },
    videoPresentation: {
        type: String,
        required: [true, "Nous vous prions d'ajouter une vidéo de présentation du domaine pour plus de visibilité"]
    },
    contacts: Array,
    emailAdress: {
        type: String,
        required: [true, "quel est l'adresse mail du domaine ?"]
    }
});


const General = mongoose.model('General', generalSchema);
module.exports = General;