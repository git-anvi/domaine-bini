const Tour = require('./../model/tourModel');
const General = require('./../model/generalModel');
const Discovery = require('./../model/discoveryModel');
const Team = require('./../model/teamModel');
const Plan = require('./../model/planModel');
const AppError = require('./../utils/appError');


exports.getHomePage = async (req, res) => {
    
    const allTours = await Tour.find();
    const generalInfo = await General.find().select('concept videoPresentation');
    const discoveries = await Discovery.find();

    res.status(200).render('home', {
        title: `Accueil | Domaine Bini`,
        allTours,
        generalInfo,
        discoveries
    })
}

exports.getAllTours = async (req, res) => {
    const discoveries = await Discovery.find();
    const tours = await Tour.find();

    res.status(200).render('tours', {
        title: `Nos circuits | Domaine Bini`,
        tours,
        discoveries
    })
}

exports.getTour = async (req, res, next) => {
    const discoveries = await Discovery.find();
    const oneTour = await Tour.find({slug: req.params.slug});
    const tourPlans = await Plan.find({tour: oneTour[0]._id}).select('imgCover description formula')

    if(!oneTour || oneTour.length === 0){
        return next(new AppError('La page à laquelle vous tentez d\'accéder est introuvable', 404))
    }

    res.status(200).render('tour-plans', {
        title: `${oneTour[0].name} | Domaine Bini`,
        oneTour,
        tourPlans,
        discoveries
    })
}

exports.getTourPlan = async (req, res) => {
    const discoveries = await Discovery.find();
    const oneTour = await Tour.find({slug: req.params.slug}).select('name');
    const tourPlan = await Plan.find({tour: oneTour[0]._id,formula: req.params.formula })


    res.status(200).render('plan-details', {
        title: `${oneTour[0].name} | Domaine Bini`,
        oneTour,
        tourPlan,
        discoveries
    })
}

exports.getDiscovery = async (req, res, next) => {
    const discoveries = await Discovery.find();
    const onediscovery = await Discovery.find({slug: req.params.slug});

    if(!onediscovery || onediscovery.length === 0){
        return next(new AppError('La page à laquelle vous tentez d\'accéder est introuvable', 404))
    }


    res.status(200).render('discovery', {
        title: `${onediscovery[0].title} | Domaine Bini`,
        onediscovery,
        discoveries
    })
}

exports.getContact = async (req, res) => {
    const discoveries = await Discovery.find();
    const general = await General.find().select('contacts emailAdress');

    res.status(200).set("Content-Security-Policy", " script-src 'self' https://www.google.com/ 'unsafe-inline' 'unsafe-eval'").render('contact', {
        title: `Contact | Domaine Bini`,
        general,
        discoveries
    })
}

exports.getAbout = async (req, res) => {
    const discoveries = await Discovery.find();
    const teamMembers = await Team.find();

    res.status(200).render('about', {
        title: `Notre équipe | Domaine Bini`,
        teamMembers,
        discoveries
    })
}

exports.reservationPage = async (req, res) => {
    const allPlans = await Plan.find().populate({path: 'tour', select: 'name'});
    const discoveries = await Discovery.find();

    res.status(200).render('reservation', {
        title: `Reservation | Domaine Bini`,
        allPlans,
        discoveries
    })
}