const catchAsync = require('./../utils/catchAsync');
const Reservation = require('./../model/reservationModel');


exports.newReservation = catchAsync(async (req, res, next) => {

    const reservation = await Reservation.create({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        cellPhone: req.body.cellPhone,
        numberOfPersons: req.body.numberOfPersons,
        dateOfReservation: req.body.dateOfReservation,
        message: req.body.message,
        reservedPlan: req.body.reservedPlan
    }); 

    res.status(201).json({
        status: 'success',
        data: {
            reservation
        }
    })

});
