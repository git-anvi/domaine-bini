const express = require('express');
const reservationController = require('../controller/reservationController');

const router = express.Router();

router.post('/', reservationController.newReservation);

module.exports = router