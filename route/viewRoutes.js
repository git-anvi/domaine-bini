const express = require('express');
const viewController = require('./../controller/viewController');

const router = express.Router();

router.get('/', viewController.getHomePage)
router.get('/about', viewController.getAbout)
router.get('/contact', viewController.getContact)
router.get('/tours', viewController.getAllTours)
router.get('/reservation', viewController.reservationPage)


router.get('/tours/:slug', viewController.getTour)
router.get('/tours/:slug/:formula', viewController.getTourPlan)
router.get('/discovery/:slug', viewController.getDiscovery)


module.exports = router